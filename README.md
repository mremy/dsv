# Delimiter Separated Values

## Description

I sometime face (scraped) data with a mix of encoding accross lines, utf8 on
one, then latin1. And the separator change too.

I don't take into account the bad lines yet, with varying separator, unclosed
chain, which breaks the flow, usw.

After looking around, I didn't find an utility managing this, so I decided to
make a small POC.

Of course, I heard about
[uchardet](https://gitlab.freedesktop.org/uchardet/uchardet) and
[chardet](https://docs.rs/chardet), but here, encoding are at the line, even
field level, so it's hard to use, at the first step.

Let's do a first draft.

Consider the single german word üben. The letter ü is available in Latin-1
(or ISO-8859-1) with code 0xFD and as for Unicode value U+00FD, as it's was
desigend to be compatible with this part of existing representation.

Let's see:
```
$ file tests/*.txt
tests/latin1.txt: ISO-8859 text
tests/utf8.txt:   Unicode text, UTF-8 text
$ hexdump -C tests/latin1.txt
00000000  fc 62 65 6e 0a                                    |.ben.|
00000005
$ hexdump -C tests/utf8.txt
00000000  c3 bc 62 65 6e 0a                                 |..ben.|
00000006
```
The latin1 file size is 5 bytes, the utf8 one 6, because U+00FD is encoded as
0xC3 0xBC.

### Separators
The following caracters may be separtors: ',',';',':' and maybe '\#'.

### Fields
They can be defined by something between two separators (\[^$\] act as
separators here). A field may be surrounded by single or double quote.

## License
TBD

## Project status
POC
